$(function(){
	//アスクル仕様にタグの構成を変更
	editTag();

	//各種CSS、JS読み込み
	readFiles();

	//ヘッダー挿入
	addHeader();

	//ぱんくず挿入
	addPankuzu();

	//サイドメニュー挿入
	addSideMenu();

	//モーダル挿入
	addModal();

	//フッター挿入
  addFooter();
  
  //ページトップへボタン挿入
  addPageTop();

	// スムーズスクロール
	smoothScroll();

  //新着情報カスタマイズ
  // topics();

  //新着情報の日付フォーマット変更
  // replaceDateFormat();

	//default.css メディアタイプ変更（印刷対応）
	cssChangeMediaType();

	//社名検索カスタマイズ
  customCompanySearch();
  
	//Bodyにクラス追加
	addClassToElements();
});


//JS実行時までの表示崩れ解消
$(window).load(function () {
	$("body").show();
});


//タグの構成変更
function editTag() {
	// var url = location.pathname;
	$('.Header').remove();
  $('.Wrapper').removeClass().addClass('wrap').attr('id', 'main-wrap');
	$('.ThemeHead').remove();
	$('.News').remove();
	$('.ThemeItem-title').remove();
	$('.Topics-items').removeClass().addClass('main');
	$('.ThemeItem').removeClass().wrapAll('<div class="contents"></div>');
	$('.ThemeItem-leftWrapper').removeClass();
	$('.ThemeItem-description').removeClass();
	$('.ThemeItem-images').remove();
	$('.ThemeItem-company').remove();
	$('.Enquetes').remove();
	$('.ResponseItem').remove();
	$('.Topics').removeClass().addClass('bodyMain');
	$('.Topics-contents').prependTo('.bodyMain');
}


//各種CSS、JS読み込み
function readFiles() {
  var themeId = getThemeId();
  var path = location.pathname;
	var cssElm = '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/css/default.css" type="text/css">'+
	'<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">';
  if (path.match(/\/ja\/?$/)) { //トップページ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/css/top.css" type="text/css">'; 
	}
	if (!path.match(/\/ja\/?$/) && themeId != "112" && themeId != "108" && themeId != "109" && themeId != "152" && themeId != "156" && themeId != "153" && !(themeId >= 138 && themeId <= 146) && themeId != 172 && themeId != 173) { //サイドバーを読み込まないページを除外
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/css/sub.css" type="text/css">';
	}
	if (themeId == "112") { //トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/css/message.css" type="text/css">';
	}
	if (themeId == "173") { //2019トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2019/css/message.css" type="text/css">';
	}
	if (themeId == "138") { //2018トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2018/css/message.css" type="text/css">';
	}
	if (themeId == "139") { //2017トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2017/css/message.css" type="text/css">';
	}
	if (themeId == "140") { //2016トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2016/css/message.css" type="text/css">';
	}
	if (themeId == "141") { //2015トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2015/css/message.css" type="text/css">';
	}
	if (themeId == "142") { //2014トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2014/css/message.css" type="text/css">';
	}
	if (themeId == "143") { //2013トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2013/css/message.css" type="text/css">';
	}
	if (themeId == "144") { //2012トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2012/css/message.css" type="text/css">';
	}
	if (themeId == "145") { //2011トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2011/css/message_2011.css" type="text/css">';
	}
	if (themeId == "146") { //2010トップメッセージ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/message/2010/css/message_2010.css" type="text/css">';
	}
	if (themeId == "165" || themeId == "167") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/sustainability/css/sustainability.css" type="text/css">';
  }
	if (themeId == "106") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/management/css/ems.css" type="text/css">';
  }
	if (themeId == "108") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/css/opinion.css" type="text/css">';
  }
	if (themeId == "90" || themeId == "91" || themeId == "94") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/environment/css/goals.css" type="text/css">';
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/environment/css/model.css" type="text/css">';
  }
	if (themeId == "174") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/environment/css/tcfd.css" type="text/css">';
  }
	if (themeId == "93") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/environment/css/env.css" type="text/css">';
  }
	if (themeId == "92") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/pb100/css/style.css" type="text/css">';
  }
	if (themeId == "97") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/environment/promise/css/promise.css" type="text/css">';
  }
	if (themeId == "162") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/environment/css/step.css" type="text/css">';
  }
	if (themeId == "101" || themeId == "103") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/social/css/social.css" type="text/css">';
  }
	if (themeId == "100") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/social/css/communication.css" type="text/css">';
  }
	if (themeId == "98" || themeId == "99") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/social/css/work.css" type="text/css">';
  }
	if (themeId == "175") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/social/css/consumer.css" type="text/css">';
  }
	if (themeId == "103") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/social/css/action.css" type="text/css">';
  }
	if (themeId == "110") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/top.css" type="text/css">';
  }
	if (themeId >= 113 && themeId <=137 || themeId == "154" || themeId == "157" || themeId >= 169 && themeId <=172 || themeId == 176|| themeId == 177) {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/css/special.css" type="text/css">';
  }
	if (themeId == "113") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/phasefree.css" type="text/css">';
  }
	if (themeId == "114" || themeId == "115" || themeId == "125" || themeId == "128" || themeId == "154" || themeId == 170 || themeId == 176 || themeId == 177) {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/support.css" type="text/css">';
  }
	if (themeId == 176 || themeId == 177) {
    cssElm = cssElm + '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">';
  }
	if (themeId == "116" || themeId == "120" || themeId == "121") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/coffee.css" type="text/css">';
  }
	if (themeId == "117") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/noplastic.css" type="text/css">';
  }
	if (themeId == "118" || themeId == "119" || themeId == "122" || themeId == "124") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/cio.css" type="text/css">';
  }
	if (themeId == "126") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/paper.css" type="text/css">';
  }
	if (themeId == "129") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/lohacodrug.css" type="text/css">';
  }
	if (themeId == "130") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/note.css" type="text/css">';
  }
	if (themeId == "132") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/lohaco.css" type="text/css">';
  }
	if (themeId == "133") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/developer.css" type="text/css">';
  }
	if (themeId == "157") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/talk.css" type="text/css">';
  }
	if (themeId == "169") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/air.css" type="text/css">';
  }
	if (themeId == "171") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/tsushima.css" type="text/css">';
  }
	if (themeId == "172") {
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/css/comebag.css" type="text/css">';
  }
	if (themeId == "109") { //ダウンロード
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/download/css/download.css" type="text/css">';
  }
	if (themeId == "152") { //このWebサイトにおける報告概要について
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/report/css/report.css" type="text/css">';
  }
	if (themeId == "156") { //過去のトピックス
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/topics/css/topics.css" type="text/css">';
  }
	if (themeId == "153") { //サイトマップ
    cssElm = cssElm + '<link rel="stylesheet" href="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/sitemap/css/sitemap.css" type="text/css">';
  }
  $(cssElm).appendTo('head');

	var jsElmHead = '<script src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/js/common.js"></script>'
	if (themeId == "113") {
		jsElmHead = jsElmHead + '<script src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/js/phasefree.js"></script>'
	}
	$(jsElmHead).appendTo('head');
}


//ヘッダー挿入
function addHeader() {
	var header = $(
		'<div id="headerwrap">'+
			'<div class="wrap">' + 
				'<div id="header">'+
					'<div class="hgroup">'+
						'<h1><a href="/ja/" class=""><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/img/header/logo.gif" alt="ASKUL" width="180" height="44"></a></h1>'+
						'<h2>サステナビリティ報告<br>（環境・社会活動報告）</h2>'+
					'</div><!-- .hgroup -->'+
					'<div class="right">'+
						'<ul id="hLink">'+
							'<li><a href="http://www.askul.co.jp/" rel="external">オフィス用品通販 ASKUL</a></li>'+
							'<li><a href="http://www.askul.co.jp/kaisya/" rel="external">会社案内</a></li>'+
							'<li><a href="https://www.askul.co.jp/kaisya/ir/" rel="external">IR情報</a></li>'+
						'</ul>'+
						'<form action="https://www.askul.co.jp/iqr/corpKeywordSearchResultView/" method="get">'+
							'<div id="header-search">'+
								'<p class="field"><input type="text" name="searchWord" value="" id="searchWord"></p>'+
								'<p class="submit"><input class="rollover" type="image" src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/img/header/icon-search-01.gif" alt="検索" onclick="void(this.form.submit());return false"></p>'+
							'</div>'+
							'<input type="hidden" name="page" value="1">'+
							'<input type="hidden" name="lang" value="ja">'+
						'</form>'+
					'</div>'+
					'<ul id="gNav">'+
						'<li><a href="/ja/" class="">トップ</a></li>'+
						'<li><a href="/ja/themes/112">トップメッセージ</a></li>'+
						'<li>'+
							'<a href="/ja/themes/168">基本方針・マテリアリティ</a>'+
							'<div id="gNavChild">'+
								'<div class="wrap">'+
									'<div class="gNavChildTtl">基本方針・マテリアリティ</div>'+
                  '<ul class="gNavChildList">'+
                    '<li><a class="leftGt" href="/ja/themes/165">基本方針等</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/167">マテリアリティ（重要課題）</a></li>'+
                  '</ul>'+
								'</div>'+
							'</div>'+
						'</li>'+
						'<li>'+
							'<a href="/ja/themes/149">ガバナンス</a>'+
							'<div id="gNavChild">'+
								'<div class="wrap">'+
									'<div class="gNavChildTtl">ガバナンス</div>'+
                  '<ul class="gNavChildList">'+
                    '<li><a class="leftGt" href="/ja/themes/83">コーポレート・ガバナンス</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/84">コンプライアンス</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/86">リスクマネジメント</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/85">腐敗防止・企業倫理</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/87">情報セキュリティ</a></li>'+
                    // '<li><a class="leftGt" href="/ja/themes/89">株主関連情報</a></li>'+
                    // '<li><a class="leftGt" href="/ja/themes/88">環境マネジメント</a></li>'+
                  '</ul>'+
								'</div>'+
							'</div>'+
						'</li>'+
            '<li>'+
              '<a href="/ja/themes/147" class="">環境</a>'+
							'<div id="gNavChild">'+
								'<div class="wrap">'+
									'<div class="gNavChildTtl">環境</div>'+
                  '<ul class="gNavChildList">'+
                    '<li><a class="leftGt" href="/ja/themes/90">環境経営</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/91">気候変動・脱炭素</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/174">TCFD提言に基づく情報開示</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/93">資源循環</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/94">生物多様性</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/92">環境配慮商品・サービス</a></li>'+
                    // '<li><a class="leftGt" href="/ja/themes/95">環境データ / 第三者保証</a></li>'+
                    // '<li><a class="leftGt" href="/ja/themes/96">環境関連の発行物</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/97">アスクルの「5つの約束」<br>（環境に関する具体的な取り組み）</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/160">環境マネジメントシステム</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/161">外部評価・社外との協働</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/162">環境活動のあゆみ</a></li>'+
                    '<li></li>'+
                  '</ul>'+
								'</div>'+
							'</div>'+
            '</li>'+
            '<li>'+
              '<a href="/ja/themes/148">社会</a>'+
							'<div id="gNavChild">'+
								'<div class="wrap">'+
									'<div class="gNavChildTtl">社会</div>'+
                  '<ul class="gNavChildList">'+
                    '<li><a class="leftGt" href="/ja/themes/98">働く仲間とともに</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/99">ダイバーシティ</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/100">お客様とともに</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/175">消費者志向経営の推進<br>（消費者志向自主宣言）</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/101">お取引先様との取り組み</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/102">商品および商品情報の品質</a></li>'+
                    '<li><a class="leftGt" href="/ja/themes/103">社会貢献活動</a></li>'+
                    // '<li><a class="leftGt" href="/ja/themes/104">地域コミュニティ</a></li>'+
                  '</ul>'+
								'</div>'+
							'</div>'+
            '</li>'+
						'<li><a href="/ja/themes/105">ESGデータ集</a></li>'+
						'<li><a href="/ja/themes/110">Special Issue</a></li>'+
					'</ul>'+
				'</div>'+	
			'</div>'
	);
	$("body").prepend(header);

	//グローバルナビ current設定
	//アクセスしているURLのパス取得
	var url_path = location.pathname.split(/(?=\.[^.]+$)/)[0];
	
	//グローバルナビにある項目の各ページURL
	var top_url_list = ["/ja"]; //トップ
	var msg_url_list = ["/ja/themes/112"]; //トップメッセージ
	var gov_url_list = ["/ja/themes/165","/ja/themes/167"]; //基本方針・マテリアリティ
	var gov_url_list = ["/ja/themes/83","/ja/themes/84","/ja/themes/86","/ja/themes/85","/ja/themes/87"]; //ガバナンス
	var env_url_list = ["/ja/themes/90","/ja/themes/91","/ja/themes/174","/ja/themes/93","/ja/themes/94","/ja/themes/92","/ja/themes/97","/ja/themes/160","/ja/themes/161","/ja/themes/162"]; //環境
	var soc_url_list = ["/ja/themes/98","/ja/themes/99","/ja/themes/100","/ja/themes/175","/ja/themes/101","/ja/themes/102","/ja/themes/103"]; //社会
	var dwl_url_list = ["/ja/themes/109"]; //ダウンロード
	var spl_url_list = ["/ja/themes/110","/ja/themes/113","/ja/themes/114","/ja/themes/115","/ja/themes/116","/ja/themes/117","/ja/themes/118","/ja/themes/119","/ja/themes/120","/ja/themes/121","/ja/themes/122","/ja/themes/123","/ja/themes/124","/ja/themes/125","/ja/themes/126","/ja/themes/127","/ja/themes/128","/ja/themes/129","/ja/themes/130","/ja/themes/131","/ja/themes/132","/ja/themes/133","/ja/themes/157","/ja/themes/134","/ja/themes/135","/ja/themes/136","/ja/themes/137","/ja/themes/154","/ja/themes/176","/ja/themes/177"]; //Special Issue

	//アクセス中のurlのパスからグロナビのどの項目にいるか判別
	var gnav_flg = "";
	if ($.inArray(url_path, top_url_list) != -1) {
		gnav_flg = "top";
	} else if ($.inArray(url_path, msg_url_list) != -1) {
		gnav_flg = "msg";
	} else if ($.inArray(url_path, gov_url_list) != -1) {
		gnav_flg = "sus";
	} else if ($.inArray(url_path, gov_url_list) != -1) {
		gnav_flg = "gov";
	} else if ($.inArray(url_path, env_url_list) != -1) {
		gnav_flg = "env";
	} else if ($.inArray(url_path, soc_url_list) != -1) {
		gnav_flg = "soc";
	} else if ($.inArray(url_path, dwl_url_list) != -1) {
		gnav_flg = "dwl";
	} else if ($.inArray(url_path, spl_url_list) != -1) {
		gnav_flg = "spl";
	}
	//グロナビのliタグにclass activeを追加
	if (gnav_flg == "top") {
		$('#gNav li').eq(0).addClass("active");
	} else if (gnav_flg == "msg") {
		$('#gNav li').eq(1).addClass("active");
	} else if (gnav_flg == "sus") {
		$('#gNav li').eq(2).addClass("active");
	} else if (gnav_flg == "gov") {
		$('#gNav li').eq(2).addClass("active");
	} else if (gnav_flg == "env") {
		$('#gNav li').eq(3).addClass("active");
	} else if (gnav_flg == "soc") {
		$('#gNav li').eq(4).addClass("active");
	} else if (gnav_flg == "dwl") {
		$('#gNav li').eq(5).addClass("active");
	} else if (gnav_flg == "spl") {
		$('#gNav li').eq(6).addClass("active");
	}
}


//ぱんくず挿入
function addPankuzu() {
	var japaneseFlg = examJE();
	var themeId = getThemeId();
	var path = location.pathname;
	// var url = location.href;

	if(japaneseFlg) {
		if (themeId == "112" || themeId == "138" || themeId == "142" || themeId == "143" || themeId == "144" || themeId == "145" || themeId == "146" || themeId == 173) {
			var pageTitle = 'トップメッセージ';
		} else if (themeId == "139") {
			var pageTitle = 'トップメッセージ <span><a href="/en/themes/139">&gt;&gt;Message from the President &amp; CEO（English）</a></span>';
		} else if (themeId == "140") {
			var pageTitle = 'トップメッセージ <span><a href="/en/themes/140">&gt;&gt;Message from the President &amp; CEO（English）</a></span>';
		} else if (themeId == "141") {
			var pageTitle = 'トップメッセージ <span><a href="/en/themes/141">&gt;&gt;Message from the President &amp; CEO（English）</a></span>';
		} else if (themeId == "168") {
			var pageTitle = '基本方針・マテリアリティ';
		} else if (themeId == "165") {
			var pageTitle = '<a href="/ja/themes/168">基本方針・マテリアリティ</a> &gt; 理念・方針';
		} else if (themeId == "167") {
			var pageTitle = '<a href="/ja/themes/168">基本方針・マテリアリティ</a> &gt; マテリアリティ（重要課題）';
		} else if (themeId == "149") {
			var pageTitle = 'ガバナンス';
		} else if (themeId == "83") {
			var pageTitle = '<a href="/ja/themes/149">ガバナンス</a> &gt; コーポレート・ガバナンス';
		} else if (themeId == "84") {
			var pageTitle = '<a href="/ja/themes/149">ガバナンス</a> &gt; コンプライアンス';
		} else if (themeId == "86") {
			var pageTitle = '<a href="/ja/themes/149">ガバナンス</a> &gt; リスクマネジメント';
		} else if (themeId == "85") {
			var pageTitle = '<a href="/ja/themes/149">ガバナンス</a> &gt; 腐敗防止・企業倫理';
		} else if (themeId == "87") {
			var pageTitle = '<a href="/ja/themes/149">ガバナンス</a> &gt; 情報セキュリティ';
		} else if (themeId == "147") {
			var pageTitle = '環境';
		} else if (themeId == "90") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; 環境経営';
		} else if (themeId == "91") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; 気候変動・脱炭素';
		} else if (themeId == "174") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; TCFD提言に基づく情報開示';
		} else if (themeId == "93") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; 資源循環';
		} else if (themeId == "94") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; 生物多様性';
		} else if (themeId == "92") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; 環境配慮商品・サービス';
		} else if (themeId == "97") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; アスクルの「5つの約束」（環境に関する具体的な取り組み）';
		} else if (themeId == "160") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; 環境マネジメントシステム';
		} else if (themeId == "161") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; 外部評価・社外との協働';
		} else if (themeId == "162") {
			var pageTitle = '<a href="/ja/themes/147">環境</a> &gt; 環境活動のあゆみ';
		} else if (themeId == "148") {
			var pageTitle = '社会';
		} else if (themeId == "98") {
			var pageTitle = '<a href="/ja/themes/148">社会</a> &gt; 働く仲間とともに';
		} else if (themeId == "99") {
			var pageTitle = '<a href="/ja/themes/148">社会</a> &gt; ダイバーシティ';
		} else if (themeId == "100") {
			var pageTitle = '<a href="/ja/themes/148">社会</a> &gt; お客様とともに';
		} else if (themeId == "175") {
			var pageTitle = '<a href="/ja/themes/148">社会</a> &gt; 消費者志向経営の推進（消費者志向自主宣言）';
		} else if (themeId == "101") {
			var pageTitle = '<a href="/ja/themes/148">社会</a> &gt; お取引先様との取り組み';
		} else if (themeId == "102") {
			var pageTitle = '<a href="/ja/themes/148">社会</a> &gt; 商品および商品情報の品質';
		} else if (themeId == "103") {
			var pageTitle = '<a href="/ja/themes/148">社会</a> &gt; 社会貢献活動';
		} else if (themeId == "105") {
			var pageTitle = 'ESG関連データ &gt; ESGデータ集';
		} else if (themeId == "106") {
			var pageTitle = 'ESG関連データ &gt; 対照表・インデックス';
		} else if (themeId == "108") {
			var pageTitle = 'ESG関連データ &gt; 第三者意見';
		} else if (themeId == "110") {
			var pageTitle = 'Special Issue';
		} else if (themeId == "110") {
			var pageTitle = 'Special Issue';
		} else if (themeId == "113") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 備えない防災のススメ　いつものオフィスが非常時にも活躍する。これからの新スタイル、フェーズフリー・オフィス。';
		} else if (themeId == "114") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; <a href="/ja/themes/154">東日本大震災復興の支援活動は10年に。これまでも、これからも。支援をずっと。</a> > ASKUL Kodomo Art Project';
		} else if (themeId == "115") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; <a href="/ja/themes/154">東日本大震災復興の支援活動は10年に。これまでも、これからも。支援をずっと。</a> > 東日本復興支援～事業応援プロジェクト～';
		} else if (themeId == "170") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; <a href="/ja/themes/154">東日本大震災復興の支援活動は10年に。これまでも、これからも。支援をずっと。</a> > 東日本復興支援（教育支援）';
		} else if (themeId == "176") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; <a href="/ja/themes/154">東日本大震災復興の支援活動は10年に。これまでも、これからも。支援をずっと。</a> > 【事業応援PJ】支援先　#001　小島製菓';
		} else if (themeId == "177") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; <a href="/ja/themes/154">東日本大震災復興の支援活動は10年に。これまでも、これからも。支援をずっと。</a> > 【事業応援PJ】支援先　#002　ANVIAN INTERNATIONAL';
		} else if (themeId == "170") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; <a href="/ja/themes/154">東日本大震災復興の支援活動は10年に。これまでも、これからも。支援をずっと。</a> > 【事業応援PJ】支援先　#002　ANVIAN INTERNATIONAL';
		} else if (themeId == "116") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; その一杯が生産国の豊かな自然と暮らしにつながる。';
		} else if (themeId == "117") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 脱プラスチックへできることから。';
		} else if (themeId == "118") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 2030年“CO2ゼロチャレンジ”を目指して';
		} else if (themeId == "119") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 地域と共生するセンターを目指して　／　AVC日高、AVC関西　稼働';
		} else if (themeId == "120") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 「おいしい」だけじゃない！ボトル缶コーヒーできました';
		} else if (themeId == "121") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 自然・働くひとにやさしい　サステナブルなコーヒー';
		} else if (themeId == "122") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 物流のイノベーションを進める／ASKUL Logistics Technology';
		} else if (themeId == "123") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 新生ASKUL LOGIST始動';
		} else if (themeId == "124") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 「お客様のための進化」を牽引するテクノロジーファーストへ／ＣＩＯインタビュー';
		} else if (themeId == "125") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; オリジナル商品における森林認証製品への取り組み';
		} else if (themeId == "126") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; コピー用紙の環境配慮の取り組み';
		} else if (themeId == "127") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 豊かなライフスタイルの実現を目指して';
		} else if (themeId == "128") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; <a href="/ja/themes/154">東日本大震災復興の支援活動は10年に。これまでも、これからも。支援をずっと。</a> > 東日本復興支援（産業復興支援）';
		} else if (themeId == "129") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 医薬品専門店「ロハコドラッグ」';
		} else if (themeId == "130") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 手づくりノートお届けレポート';
		} else if (themeId == "131") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; ASKUL Logi PARK 首都圏';
		} else if (themeId == "132") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 働く女性を応援する「LOHACO」、スタート。';
		} else if (themeId == "133") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; お客様のホンネをホンキでかたちにする「お仕事専用 こんなのほしいな開発部」';
		} else if (themeId == "157") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 「お仕事専用 こんなのほしいな開発部」教えて！開発部のお仕事';
		} else if (themeId == "134") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 「お仕事専用 お客様からの信頼回復と新しいアスクルの誕生';
		} else if (themeId == "135") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; ECO-TURN誕生秘話';
		} else if (themeId == "136") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; オリジナル商品へのこだわり';
		} else if (themeId == "137") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; SOLOEL事業の環境効果';
		} else if (themeId == "154") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 東日本大震災復興の支援活動は10年に。これまでも、これからも。支援をずっと。';
		} else if (themeId == "169") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; 空気や水の環境を考えるプロジェクト';
		} else if (themeId == "171") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; TSUSHIMA×ASKUL project';
		} else if (themeId == "172") {
			var pageTitle = '<a href="/ja/themes/110">Special Issue</a> &gt; アスクルカタログが紙袋になってCome bag（カムバッグ）';
		} else if (themeId == "109") {
			var pageTitle = 'ダウンロード';
		} else if (themeId == "152") {
			var pageTitle = 'このWebサイトにおける報告概要について';
		} else if (themeId == "156") {
			var pageTitle = '過去のトピックス';
		} else if (themeId == "153") {
			var pageTitle = 'サイトマップ';
		}


	} else if (themeId == "138") {
		var pageTitle = 'Message from the President & CEO<span><a href="/ja/themes/138">&gt;&gt;日本語</a></span>';
	} else if (themeId == "139") {
		var pageTitle = 'Message from the President & CEO<span><a href="/ja/themes/139">&gt;&gt;日本語</a></span>';
	} else if (themeId == "140") {
		var pageTitle = 'Message from the President & CEO<span><a href="/ja/themes/140">&gt;&gt;日本語</a></span>';
	} else if (themeId == "141") {
		var pageTitle = 'Message from the President & CEO<span><a href="/ja/themes/141">&gt;&gt;日本語</a></span>';
	}

	if(japaneseFlg){
		if(path.match(/\/ja\/?$/)) {
			var siteNav = $();
		} else {
			var siteNav = $(
				'<div id="breadCrumbs">'+
					'<p>'+
						'<a href="/ja">トップページ</a> &gt; '+
						pageTitle +
					'</p>'+
				'</div>'
			);
		}
	} else {
		var siteNav = $(
				'<div id="breadCrumbs">'+
					'<p>'+
						'<a href="/en">Top Page</a> &gt; '+
						pageTitle +
					'</p>'+
				'</div>'
			);
	}

	$("#main-wrap").prepend(siteNav);
}


//サイドメニュー挿入
function addSideMenu() {
	var themeId = getThemeId();
	var path = location.pathname;
	// var url = location.href;

	//サイドメニューなし
	if (path.match(/\/ja\/?$/) || themeId == "112") {
		var sidemenu = $();
	//サイドメニュー（基本方針・マテリアリティ）
	} else if (themeId == "168" || themeId == "165" || themeId == "167") {
		var sidemenu = $(
			'<div class="snav">'+
				'<h2><a href="/ja/themes/168">基本方針・<br>マテリアリティ</a></h2>'+
				'<ul>'+
					'<li><a href="/ja/themes/165">基本方針等</a></li>'+
					'<li><a href="/ja/themes/167">マテリアリティ（重要課題）</a></li>'+
				'</ul>'+
			'</div>'
		);
	//サイドメニュー（ガバナンス）
	} else if (themeId == "149" || themeId == "83" || themeId == "84" || themeId == "86" || themeId == "85" || themeId == "87") {
		var sidemenu = $(
			'<div class="snav">'+
				'<h2><a href="/ja/themes/149" class="">ガバナンス</a></h2>'+
				'<ul>'+
					// '<li><a href="/ja/themes/76">理念・方針</a></li>'+
					// '<li><a href="/ja/themes/82">CSR・サステナビリティ</a></li>'+
					'<li><a href="/ja/themes/83">コーポレート・ガバナンス</a></li>'+
					'<li><a href="/ja/themes/84">コンプライアンス</a></li>'+
					'<li><a href="/ja/themes/86">リスクマネジメント</a></li>'+
					'<li><a href="/ja/themes/85">腐敗防止・企業倫理</a></li>'+
					'<li><a href="/ja/themes/87">情報セキュリティ</a></li>'+
					// '<li><a href="#"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/img/snav/snav_man04_off.gif" alt="環境マネジメントシステム" width="156" height="41"></a></li>'+
					// '<li><a href="#"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/img/snav/snav_man07_off.gif" alt="対照表・インデックス" width="156" height="41"></a></li>'+
				'</ul>'+
				'<p>'+
				'<a href="/ja/themes/168"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/img/rlink_banner10.png" alt="アスクルのサステナビリティ"></a>'+
				'</p>'+
			'</div>'
		);
	//サイドメニュー（環境）
	} else if (themeId == "147" || themeId == "90" || themeId == "91" || themeId == "174" || themeId == "93" || themeId == "94" || themeId == "92" || themeId == "97" || themeId == "160" || themeId == "161" || themeId == "162") {
		var sidemenu = $(
			'<div class="snav">'+
				'<h2><a href="/ja/themes/147">環境</a></h2>'+
				'<ul>'+
					'<li><a href="/ja/themes/90">環境経営</a></li>'+
					'<li><a href="/ja/themes/91">気候変動・脱炭素</a></li>'+
					'<li><a href="/ja/themes/174">TCFD提言に基づく情報開示</a></li>'+
					'<li><a href="/ja/themes/93">資源循環</a></li>'+
					'<li><a href="/ja/themes/94">生物多様性</a></li>'+
					'<li><a href="/ja/themes/92">環境配慮商品・サービス</a></li>'+
					'<li><a href="/ja/themes/97">アスクルの「5つの約束」<br>（環境に関する具体的な取り組み）</a></li>'+
					'<li><a href="/ja/themes/160">環境マネジメントシステム</a></li>'+
					'<li><a href="/ja/themes/161">外部評価・社外との協働</a></li>'+
					'<li><a href="/ja/themes/162">環境活動のあゆみ</a></li>'+
				'</ul>'+
				// '<p><a href="#"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/pb100/img/pb100_bnr_01_off.gif" alt="アスクルのオリジナル商品の環境対応 ASKULオリジナル" width="180" height="77"></a></p>'+
				'<p><a href="http://www.info.askul.co.jp/reddplus/" rel="external"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/img/rlink_banner08.gif" alt="ふやして減らそう！住友林業 × アスクル × ヤンマー" width="178" height="48"><br>地球温暖化を防ぐ「REDD＋」の取り組みをはじめています</a></p>'+
			'</div>'
		);
	//サイドメニュー（社会）
	} else if (themeId == "148" || themeId == "98" || themeId == "99" || themeId == "100" || themeId == "175" || themeId == "101" || themeId == "102" || themeId == "103") {
		var sidemenu = $(
			'<div class="snav">'+
				'<h2><a href="/ja/themes/148">社会</a></h2>'+
				'<ul>'+
					'<li><a href="/ja/themes/98">働く仲間とともに</a></li>'+
					'<li><a href="/ja/themes/99">ダイバーシティ</a></li>'+
					'<li><a href="/ja/themes/100">お客様とともに</a></li>'+
					'<li><a href="/ja/themes/175">消費者志向経営の推進<br>（消費者志向自主宣言）</a></li>'+
					'<li><a href="/ja/themes/101">お取引先様との取り組み</a></li>'+
					'<li><a href="/ja/themes/102">商品および商品情報の品質</a></li>'+
					'<li><a href="/ja/themes/103">社会貢献活動</a></li>'+
				'</ul>'+
			'</div>'
		);
	//サイドメニュー（ESG関連データ）
	} else if (themeId == "105" || themeId == "106") {
		var sidemenu = $(
			'<div class="snav">'+
				'<h2><a href="#" class="">ESG関連データ</a></h2>'+
				'<ul>'+
					'<li><a href="/ja/themes/105">ESGデータ集</a></li>'+
					'<li><a href="/ja/themes/106">対照表・インデックス</a></li>'+
				'</ul>'+
			'</div>'
		);
	//サイドメニュー（Special Issue）
	} else if (themeId >= 113 && themeId <= 137 || themeId == "154" || themeId == "157" || themeId >= 169 && themeId <= 172 || themeId == 176|| themeId == 177) {
		var sidemenu = $(
			'<div class="spnav">'+
			'<h2><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/sidetitle.gif" alt="その他の Special Issue" width="150" height="22" /></h2>'+
			'<ul>'+
			'<li class="exLink"><a href="/ja/themes/115"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/spmn36_off.jpg" alt="東日本復興支援" width="158" height="78" /></a>'+
			'アスクルオリジナルデザイン商品のご購入金額の一部が東日本復興支援につながります。</li>'+
			'<li class="exLink"><a href="/ja/themes/114"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/spmn35_off.jpg" alt="ASKUL Kodomo Art Project" width="158" height="93" /></a>'+
			'“ASKUL Kodomo Art Project”とは子どもたちの描く絵を用いて、子どもたちを支援する活動をサポートするプロジェクトです。</li>'+
			'<li class="exLink"><a href="/ja/themes/170"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/spmn37_off.jpg" alt="東日本復興支援（教育支援）" width="158" /></a>アスクルオリジナルデザイン商品のご購入金額の一部が東日本復興支援につながります。（東北の人材育成・教育を支援）</li>'+
			'<li class="exLink"><a href="/ja/themes/169"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/spmn40_off.jpg" alt="空気や水の環境を考えるプロジェクト" width="158" /></a>対象商品のご購入金額の一部が、空気や水の環境改善に取り組む団体の活動支援に役立てられます。（エステー株式会社とアスクルによる共同プロジェクト）</li>'+
			'<li class="exLink"><a href="/ja/themes/171"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/spmn38_off.jpg" alt="TSUSHIMA×ASKUL project" width="158" /></a>対馬市とともに取り組む、サーキュラーエコノミーと海洋プラスチックごみ対策のための「 TSUSHIMA×ASKUL project」のご紹介です。</li>'+
			'<li class="exLink"><a href="/ja/themes/172"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/spmn39_off.jpg" alt="アスクルカタログが紙袋になってCome bag（カムバッグ）" width="158" /></a>アスクルカタログが生まれ変わった紙袋「Come bag（カムバッグ）」で、新しい資源循環の取り組みを始めます。</li>'+
			'<li class="exLink"><a href="/ja/themes/113"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/spmn45_off.jpg" alt="2030年“「フェーズフリー商品」なら、日常時も非常時も役に立つ！" width="158" height="92" /></a>いつも使っているアイテムが、非常時でも役に立って、みんなの安全を守る。新しい防災のかたち「フェーズフリー商品」をご紹介します。</li>'+
			'</ul>'+
			'<div id="archive"><a href="/ja/themes/110"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/archive_page_off.gif" alt="もっと見る" width="64" height="13" /></a></div>'+
			'</div>'+
			'<!-- .spnav -->'
			);
	}
	$(".main").after(sidemenu);

	//サイドメニュー current設定
	var url_path = location.pathname.split(/(?=\.[^.]+$)/)[0];
	$('.snav').children('ul').children('li').children('a').each(function(i, elem) {
		if (url_path == $(elem).attr('href')) {
			$(elem).parent('li').addClass('active');
		}
	});
}

//モーダル挿入
function addModal() {
	var themeId = getThemeId();
	if (themeId == "113") {
		var modal = $(
		'<div id="modal" class="modal">'+
		'<div class="modal_closebtn"><span><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/phasefree_modal_btn.gif" alt=""></span></div>'+
		'<div class="modal_inr">'+
		'<img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/special/img/phasefree_modal_img.png" alt="">'+
		'</div>'+
		'</div>'+
		'<div id="modalbg" class="modalbg"></div>'
		);
		$(".bodyMain").after(modal);
	}
}

//フッター挿入
function addFooter() {
	var footer = $(
	'<div id="footerwrap">'+
    '<div class="wrap">'+
      '<div class="footer">'+
        '<div class="footerTtl">'+
          '<a href="/ja/"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/img/footer/footer_logo.png" alt="ASKULロゴ">'+
          'サステナビリティ報告（環境・社会活動報告）</a>'+
        '</div>'+
        '<div class="footerReport leftGt"><a href="/ja/themes/152">このWebサイトにおける報告概要について</a></div>'+
        '<div class="footerLinkWrap">'+
          '<ul class="footerLink">'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/112">トップメッセージ</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/168">基本方針・マテリアリティ</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/149">ガバナンス</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/147">環境</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/148">社会</a></li>'+
          '</ul>'+
          '<ul class="footerLink">'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/110">Special Issue</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/105">ESGデータ集</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/106">対照表・インデックス</a></li>'+
            // '<li class="footerLinkItem leftGt"><a href="/ja/themes/107">社外からの評価等</a></li>'+
            // '<li class="footerLinkItem leftGt"><a href="/ja/themes/108">第三者意見・第三者保証</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/109">ダウンロード</a></li>'+
          '</ul>'+
          '<ul class="footerLink">'+
            '<li class="footerLinkItem leftGt"><a href="/ja/themes/153">サイトマップ</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="https://www.askul.co.jp/iqr/corpInquiryInputView/?inqKbn=4" rel="external">お問い合わせ</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="https://www.askul.co.jp/guide/policy/privacy.html" rel="external">プライバシーポリシー</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="https://www.askul.co.jp/guide/policy/browser/accessibility.html" rel="external">ご利用環境について</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="https://www.askul.co.jp/etc/disclaimer.html" rel="external">ご利用上の注意</a></li>'+
          '</ul>'+
          '<ul class="footerLink">'+
            '<li class="footerLinkItem footerBanner"><a href="http://www.askul.co.jp/" rel="external"><img src="https://sustainability-cms-staging5-7-s3.s3-ap-northeast-1.amazonaws.com/csr/common/img/footer/footer_banner.png" alt="オフィス用品通販 ASKUL"><br>オフィス用品通販 ASKUL</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="http://www.askul.co.jp/kaisya/index.html" rel="external">会社情報</a></li>'+
            '<li class="footerLinkItem leftGt"><a href="https://www.askul.co.jp/kaisya/ir/" rel="external">IR情報</a></li>'+
          '</ul>'+
        '</div>'+
      '</div>'+
      '</div>'+
    '<p class="copyright">&copy; ASKUL Corporation. All rights reserved.</p>'
	);
	$("#main-wrap").after(footer);
}


// ページトップへボタン挿入
function addPageTop(){
  $('body').append('<a href="javascript:void(0);" class="pageTop"></a>');
  var pageTop = $('.pageTop');
  pageTop.on('click', function() {
    $('html, body').animate({scrollTop: '0'}, 500);
  });

  $(window).on('load scroll resize', function(){
    var scrollHeight = $(document).height();
    var scrollPosition = $(window).height() + $(window).scrollTop();
    var footerHeight = $('#footerwrap').innerHeight();
    if( scrollHeight - scrollPosition <= footerHeight ) {
      pageTop.css({
        "position": "absolute",
        "bottom": footerHeight + 20
      });
    } else {
      pageTop.css({
        "position": "fixed",
        "bottom": "20px"
      });
    }
  });
};

// スムーズスクロール
function smoothScroll(){
	var urlHash = location.hash;
	if(urlHash) {
			$('body,html').stop().scrollTop(0);
			setTimeout(function(){
					var target = $(urlHash);
					var position = target.offset().top;
					$('body,html').stop().animate({scrollTop:position}, 500);
			}, 500);
	}
	$('a[href^="#"]').click(function() {
			var href= $(this).attr("href");
			var target = $(href);
			var position = target.offset().top;
			$('body,html').stop().animate({scrollTop:position}, 500);   
	});
}

//新着情報カスタマイズ
function topics() {
  $('.News h3').remove('');
  $('.News').prepend('<h2 class="topTopicsH2"><span class="btmBg">トピックス</span></h2>');
  $('.News-list dd').addClass('leftGt');
  $('.News').append('<div class="topTopicsPast"><a class="leftGt" href="#">過去の一覧はこちら</a></div>');
}

//新着情報の日付フォーマット変更
function replaceDateFormat() {
  $('.News-list dt').each(function(){
    var txt = $(this).text();
    $(this).text(
      txt.replace(/\//g, '.')
    )
  });
}

//default.css メディアタイプ変更（印刷対応）
function cssChangeMediaType() {
	$("link")[1].media = "all";
}


//社名検索カスタマイズ
function customCompanySearch() {
	$('select#company_ids option[value=1]').remove(); //株式会社ディ・エフ・エフ
}


//日英判別
function examJE() {
	var url = location.href;
	var japaneseFlg;
	if (url.match(/\/ja\/?/)) {
		japaneseFlg = true;
	} else if (/\/en\/?/) {
		japaneseFlg = false;
	}
	return japaneseFlg;
}


//URLからテーマID取得
function getThemeId () {
	var path = location.pathname.split(/(?=\.[^.]+$)/)[0];
	path = path.substring(path.indexOf("themes/") + 7, path.length);
	return path;
}

//Bodyにクラス追加
function addClassToElements() {
  var path = location.pathname;
	if (path.match(/\/ja\/?$/)) { 
		$('body').addClass('top');
	}
}
